module SimplePlot

greetSimplePlot() = print("Hello World! -from SimplePlot")
export greetSimplePlots


using Statistics		# Mean
# using LaTeXStrings      # nice LaTeX strings
# --- Plot modules
using Plotly			# Fancy plot
using Plots
using PGFPlotsX			# Latex plot
using GR				# Why chosing only one plot when you can use them all ?
using Colors
using FileIO
#using ORCA
using Dates
using JLD2				# Saving
using Infiltrator		# Step point
using Printf

using FFTW
using DSP				# Resample, and DSP utils
using SlidingDFT		# https://gitlab.inria.fr/clavaud/SlidingDFT.jl







function plot2(data,name="")
	if length(data) > 800000
		gr();
	else
		plotly();
	end

	# --- Figure
	layout = Plotly.Layout(;title=name,
					xaxis_title="",
					yaxis_title="",
					xaxis_showgrid=true, yaxis_showgrid=true,
					#xaxis_range = [0,3000],
					#legend_y=1.15, legend_x=0.7,
					);
	pl1	  = Plotly.scatter(; x= 1:length(data),y=data , name=name);
	plt = Plotly.plot([pl1],layout);
	display(plt);
end
export plot2





function plotList(data,name; vli=[], save=[])
@assert size(data,1) == size(name,1)

# pgfplots()

# @info "vers1.4"
mm = maximum(maximum(data[:]));
len = size(data,1);
	# --- Figure

	#https://plotly.com/javascript/configuration-options/#hide-the-modebar-with-plotly.js
	config=Dict(:staticPlot => false,
                        :showTips =>false,
                        :displaylogo => false,
                        :scrollZoom => true,
                        :editable => true,
                        :responsive => true,
                        :showEditInChartStudio => true,
                        :plotlyServerURL => "https://chart-studio.plotly.com"
                        );
# 	config=AbstractDict("showTips"=> false, "displaylogo"=> false)
# 	@infiltrate

	layout = Plotly.Layout(;
                    xaxis_title="",
                    yaxis_title="",
                    xaxis_showgrid=true,
                    yaxis_showgrid=true,
                    showTips=false,
					# yaxis_type="log",
					# xaxis_tickmode = "array",
					# xaxis_tickvals = [16, 8, 4, 2, 9],
					);

	pl = PlotlyBase.GenericTrace{Dict{Symbol,Any}}[];
	for ii = 1 : len
        pl1 = Plotly.scatter(; x= 1:length(data[ii]),y=data[ii] , name=name[ii],line_width=ii) #1:length(data[ii,:]);
		push!(pl, pl1);
	end

	if isempty(vli)==false
		x0 = vli
		y0 = [mm]
		x1 = vli
		y1 = [0]
		shapes = line(x0, x1, y0, y1, line_width=0.5, line_dash="dashdot");
		layout = Plotly.Layout(;
						xaxis_title="",
						yaxis_title="",
						xaxis_showgrid=true, yaxis_showgrid=true,
						shapes=shapes
						);
	end
	plt = Plotly.plot(pl,layout,options=config);
	display(plt);

	if save!=[]
        Plotly.savefig(plt,save*".svg")
	end
end
export plotList













#
# function plotListTex(data,name; vli=[], save=[])
# 	@assert size(data,1) == size(name,1)
#
# 	pgfplots()
#
# 	# @info "vers1.4"
# 	mm = maximum(maximum(data[:]));
# 	len = size(data,1);
# 		# --- Figure
# 		a = @pgf TikzPicture({ "scale" => 1.5 }, Axis({
# 			height      ="3in",
# 			width       ="4in",
# 			grid,
# 			yminorgrids,
# 			xminorgrids,
# 			xlabel      = "Sample",
# 			ylabel      = "Channel index",
# 			title       = "",
# 			ymin		= 0,
# 			ymax		= 30,
# 			# legend_style="{at={(1,0)},anchor=south west,legend cell align=left,align=left,draw=white!15!black}"
# 			},
# 			))
#
#
# 			col = ["gray" "teal" "cyan" "black" "gray" "cyan" "black"]
#
# 		for ii = 1 : len
# 	        pl1 = Plotly.scatter(; x= 1:length(data[ii]),y=data[ii] , name=name[ii]) #1:length(data[ii,:]);
# 			push!(pl, pl1);
# 		end
#
# 		if isempty(vli)==false
# 			x0 = vli
# 			y0 = [mm]
# 			x1 = vli
# 			y1 = [0]
# 			shapes = line(x0, x1, y0, y1, line_width=0.5, line_dash="dashdot");
# 			layout = Plotly.Layout(;
# 							xaxis_title="",
# 							yaxis_title="",
# 							xaxis_showgrid=true, yaxis_showgrid=true,
# 							shapes=shapes
# 							);
# 		end
# 		plt = Plotly.plot(pl,layout,options=config);
# 		display(plt);
#
# 		if save!=[]
# 	        Plotly.savefig(plt,save*".svg")
# 		end
# 	end
# export plotListTex









function plot3(x,y,name)
	# --- Figure
	layout = Plotly.Layout(;title=name,
						   xaxis_title="",
					yaxis_title="",
					xaxis_showgrid=true, yaxis_showgrid=true,
					#xaxis_range = [0,3000],
					#legend_y=1.15, legend_x=0.7,
					);
	pl1	  = Plotly.scatter(; x=x ,y=y , name=name);
	plt = Plotly.plot([pl1],layout)
	display(plt);
end
export plot3

function plotPerio(data,name,fs)
	# --- Figure
	perio = welch_pgram(data; onesided=true, fs=fs)
	layout = Plotly.Layout(;title=name,
						   xaxis_title="Freq",
					yaxis_title="Power",
					xaxis_showgrid=true, yaxis_showgrid=true,
					#xaxis_range = [0,3000],
					#legend_y=1.15, legend_x=0.7,
					)
	pl1	  = Plotly.scatter(; x= perio.freq,y=DSP.pow2db.(perio.power) , name=name);
	plt = Plotly.plot([pl1],layout)
	display(plt);
end
export plotPerio



function plotFilter2(h)
	# Freq response
	N = length(h);
	ω = range(0, stop=π, length=N)
	H = freqz(PolynomialRatio(h, [1.0]),ω )


	# --- Figure
	layout = Plotly.Layout(;title=" Frequency response ",
	       xaxis_title="Pulsation ",
	       yaxis_title=" Magnitude ",
	       xaxis_showgrid=true, yaxis_showgrid=true,
	       )
	pl1   = Plotly.scatter(; x= ω/ (2pi)*Fe ,y= 10*log10.(abs.(H)) , name="| H | ");
	pl2   = Plotly.scatter(; x= ω/ (2pi)*Fe ,y= unwrap(angle.(H)) , name="θ");
	plt2 = Plotly.plot([pl1],layout)
	plt3 = Plotly.plot([pl2],layout)
	display([plt2,plt3]);
end
export plotFilter2

function plotFilter(h,Fe)
	# Freq response
	N = h.kernel.hLen;
	ω = range(0, stop=π, length=N)
	H = freqz(PolynomialRatio(h.h, [1.0]),ω )

	# --- Figure
	layout = Plotly.Layout(;title=" Frequency response ",
	       xaxis_title="Freq ",
	       yaxis_title=" Magnitude ",
	       xaxis_showgrid=true, yaxis_showgrid=true,
	       )
	pl1   = Plotly.scatter(; x= ω/ (2pi)*Fe ,y= 10*log10.(abs.(H)) , name="| H | ");
	pl2   = Plotly.scatter(; x= ω/ (2pi)*Fe ,y= unwrap(angle.(H)) , name="θ");
	plt2 = Plotly.plot([pl1],layout)
	plt3 = Plotly.plot([pl2],layout)
	display([plt2,plt3]);
end
export plotFilter




function plotSpectrum(Fs,sig)
	pading = max(0,2048 - length(sig))
	sig2 = [sig ; zeros(eltype(sig),pading)]
	sigFFT	= abs2.(fftshift(fft(sig2)));
	xAx		= ((0:length(sigFFT)-1)./length(sigFFT) .- 0.5)  .* Fs;
	# --- Figure


	layout = Plotly.Layout(;title=" Spectrum",
						  xaxis_title= "Frequency",
						  yaxis_title="  Magnitude [dB] ",
						  xaxis_showgrid=true, yaxis_showgrid=true,
						  yaxis_range=[0, 160]
						  );
	pl1	= Plotly.scatter(; x=xAx ,y= 20 .*log10.(sigFFT), name="sig");
	plt = Plotly.plot([pl1],layout);
	display(plt);
end
export plotSpectrum




function plotWaterfall(Fs,sig, kind; nFFT=80, len = nFFT, fast=false, save=false, normalize=false, mini=-10000, maxi=-10000)
	FFTW.set_num_threads(1)
	@assert nFFT >= len "wrong size"

	if fast == true || length(sig) > 80000
		gr();
	else
		plotly();
	end

	totalDuration = length(sig)/Fs

	if kind == "sfft"
		win = hamming(nFFT)
		nSegment = Int(floor(length(sig)/len))
		sigFFT=zeros(nSegment,Int(nFFT))
		pading = zeros(max(0,nFFT - len))
		@inbounds for ii in 1:nSegment
			temp = sig[(ii-1)*len+1 : ii*len]
			sigFFT[ii,:]=20*log10.(abs2.(fft([temp ; pading].*win )));
			δ=totalDuration/size(sigFFT,1)
			timeAx 	= collect((δ:δ:totalDuration).*1000)
		end
	elseif kind == "sdft"
		stru = initSdft(nFFT)
		sigFFT = zeros(Float64,length(sig),Int(nFFT))
		@inbounds for ii = 1:1:length(sig)
			sigFFT[ii,:]=20*log10.(abs2.(sDFT!(stru, sig[ii]).+eps(Float64)))
		end
		sigFFT=sigFFT[1:600:end,:]
		δ=totalDuration/size(sigFFT,1)
		timeAx 	= collect((δ:δ:totalDuration).*1000)
	else
		@assert "Wrong method"
	end


	if normalize == true
		@inbounds for ii in 1:size(sigFFT,1)
			sigFFT[ii,:] = sigFFT[ii,:]/sqrt(mean(abs2.(sigFFT[ii,:]))); #Normalize
		end
    end

	freqAx	= collect(((0:1/size(sigFFT,2):1-1/size(sigFFT,2)).*(Fs))./1000/1000)
	# totalDuration = (size(sigFFT,1))/Fs*size(sigFFT,2)

# ,color=:blues
	data2 = Plots.heatmap(timeAx,freqAx, transpose(sigFFT) ;ylabel="Frequency (MHz)",xlabel="Sample",zlabel="Magnitude [dB]",color=:dense)
	plot!(size=(600,400))

	if  mini==-10000
		cmin = Int64(round(minimum(sigFFT[:])))
	else
		cmin = mini
	end
	if  maxi==-10000
		cmax = Int64(round(maximum(sigFFT[:])))
	else
		cmax = maxi
	end

	plot!(clims=(cmin,cmax))


	display(data2);
	if save == true
		# --- Force plot renderer
		datetimenow = Dates.now()
		ttt = Dates.format(datetimenow, "mm-dd HH:MM")
		Plots.savefig(data2, "Waterfall"*ttt*".pdf")
	end
end
export plotWaterfall


function plotWaterfallSIPS(Fs,sig, kind; nFFT=80, len = nFFT, fast=false, save=false, normalize=false, mini=-10000, maxi=-10000)
	FFTW.set_num_threads(1)
	@assert nFFT >= len "wrong size"

	totalDuration = length(sig)/Fs

	if kind == "sfft"
		win = hamming(nFFT)
		nSegment = Int(floor(length(sig)/len))
		sigFFT=zeros(nSegment,Int(nFFT))
		pading = zeros(max(0,nFFT - len))
		@inbounds for ii in 1:nSegment
			temp = sig[(ii-1)*len+1 : ii*len]
			sigFFT[ii,:]=20*log10.(abs2.(fft([temp ; pading].*win )));
			zerofy!(sigFFT[ii,:], mini, maxi)
		end
	elseif kind == "sdft"
		stru = initSdft(nFFT)
		sigFFT = zeros(Float64,length(sig),Int(nFFT))
		@inbounds for ii = 1:1:length(sig)
			sigFFT[ii,:]=20*log10.(abs2.(sDFT!(stru, sig[ii]).+eps(Float64)))
			zerofy!(sigFFT[ii,:], mini, maxi)
		end
	else
		@assert "Wrong method"
	end


	if normalize == true
		@inbounds for ii in 1:size(sigFFT,1)
			sigFFT[ii,:] = sigFFT[ii,:]/sqrt(mean(abs2.(sigFFT[ii,:]))); #Normalize
		end
    end

	freqAx	= ((0:1/size(sigFFT,2):1-1/size(sigFFT,2)).*(Fs))./1000/1000
	# totalDuration = (size(sigFFT,1))/Fs*size(sigFFT,2)
	δ=totalDuration/size(sigFFT,1)
	timeAx 	= (δ:δ:totalDuration)
# ,color=:blues
	# data2 = Plots.heatmap(timeAx,freqAx, transpose(sigFFT) ;xlabel="Sample",ylabel="Frequency (MHz)",zlabel="Magnitude [dB]",options=config,color=:blues)

	if  mini==-10000
		cmin = Float32(round(minimum(sigFFT[:])))
	else
		cmin = mini
	end
	if  maxi==-10000
		cmax = Float32(round(maximum(sigFFT[:])))
	else
		cmax = maxi
	end
	gr();
	a = @pgf Axis({
				height      ="3in",
				width       ="4in",
				colorbar, "colormap/thermal",
				view = (0, 90),
				yminorgrids,
				xminorgrids,
				xlabel      = "Sample",
				ylabel      = "Frequency (MHz)",
				zmin = mini,
				zmax = maxi
				},
				)
push!(a, @pgf PGFPlotsX.Plot3({surf,shader = "flat"},Table(timeAx,freqAx,sigFFT)))

	# plot!(size=(600,400))
	#

	#
	# plot!(clims=(cmin,cmax))
	# @infiltrate
	pgfsave("fig33.tex", a)

	# display(a);

	# if save == true
	# 	# --- Force plot renderer
	# 	datetimenow = Dates.now()
	# 	ttt = Dates.format(datetimenow, "mm-dd HH:MM")
	# 	display(pObj);
	# 	ORCA.savefig(data2, "Waterfall"*ttt*".pdf")
	# end
end
export plotWaterfallSIPS

function zerofy!(x, level1, level2)
    for (i, val) in enumerate(x)
        if (val) < level1
            x[i] = level1
        end
		if (val) > level2
			x[i] = level2
		end
    end
end


"""
---
# --- Syntax
plotWaterfallAnim(Fs,sigX,nbChannels, vectFH, currentBurstDur, channelBW, currentCfo)
# --- Input parameters
- sigX		 : Signal [Array{Complex{Float64}}] !! Assuming perfct time sync !!
- Fs	 	 : Sample rate, for plot purpose [Int]
- nFFT 	 	 : Frequency bin [Int]
- vectFH     : Jump index
- currentBurstDur : Number of sample per burst
- channelBW  : Channel bandwidth
- currentCfo : CFO of channel
# --- Output parameters
	None
# ---
# v 1.0 - Corentin Lavaud
"""
function plotWaterfallAnim(Fs,sigX,nbChannels, vectFH, currentBurstDur, channelBW, currentCfo; fps=120, step=10, over=1)
	over = 1
	nFFT = over*nbChannels
	sig = sigX[1:2000]
	len = Int(floor(length(sig)/currentBurstDur)*currentBurstDur)
	sig = sigX[1:len]
	@assert length(sig)>=nFFT
	stru = initSdft(nFFT, meanVal=1)
	dftAll = zeros(Float64,Int(nFFT),length(sig))
	# dftAll2 = zeros(Float64,Int(nFFT),length(sig))
	xdata = collect(0:1/over:nbChannels-1/over)
	channelVect = zeros(Float64,length(sig))
	for ii = 1:1:Int(len/currentBurstDur)
		channelVect[(ii-1)*currentBurstDur+1:(ii)*currentBurstDur] = (over.*vectFH[ii].*ones(currentBurstDur)).+currentCfo
	end
	## main loop
	@inbounds for ii = 1:1:length(sig)
		temp = sDFT!(stru, sig[ii])
		temp = 20*log10.(abs2.(temp.+eps(Float64))) #
		dftAll[:,ii]=(temp)
	end
	# @inbounds for ii = nFFT:length(sig)
	# 	dftAll2[:,ii]= 0.8.*dftAll[:,ii]+0.2.*dftAll[:,ii-1] #DONT WORKS
	# end
	plot2(abs2.(sig),"Temporelle")
	@infiltrate
	gr()
	Plots.GRBackend()

	anim = @animate for i=1:step:length(sig)
		Plots.plot(xdata, dftAll[:,i])
		inn = currentBurstDur - (i%currentBurstDur)
		# Plots.scatter(xdata, dftAll[:,i])
		Plots.vline!([channelVect[i]/over], line = (:red, :dashdot, 1))
		Plots.vline!([ (channelVect[i]+(round(over/2)))/over], line = (:grey, :dot, 0.9))
		Plots.vline!([ (channelVect[i]-(round(over/2)))/over], line = (:grey, :dot, 0.9))
		Plots.annotate!([(nbChannels-8,100,text("Frame $(i)/$(length(sig)/step)\nNext Jump in $(inn)", 14))])

		# Plots.vline!([channelVect[i]/over], line = (:grey, :dashdot, 1))
#xticks=1:1:40
		Plots.plot!(ylims=(-40,120),xlabel="Frequency bin",ylabel=" Magnitude [dB] ",xgrid=false, leg=false, label=["Spectrum","Used channel"])
		# title!("TITLE")
	end
	datetimenow = Dates.now()
	ttt = Dates.format(datetimenow, "mm-dd HH:MM")
	gif(anim, "anim_"*ttt*".gif", fps = fps)
end
export plotWaterfallAnim

function printField(struc)
    T = typeof(struc)
	siz = Base.summarysize(struc; chargeall= Any)
	@printf("\n%20s %40s %15s Bytes\n", "fieldname", "type", "size")
	@printf("-----------------------------------------------------------------------------------\n")
	@printf("%60s %15s Bytes\n", T, siz)
	@printf("-----------------------------------------------------------------------------------\n")

	for (name, typ) in zip(fieldnames(T), T.types)
		pre = summary(eval(:($(struc).$(name))))
		siz = Base.summarysize( eval(:($(struc).$(name))) )
		#sym = size(pre)#pre = size(getfield(struc,sym))
		#println("type of the fieldname $name \t\t\t is $pre \t\t\t of $siz Bytes")
		@printf("%20s %40s %15s Bytes\n", name, pre, siz)
	end
	@printf("\n")
end
export printField



function getColorLatex(index,size=1);
	if size < 5
		# ----------------------------------------------------
		# --- Manual color generation
		# ----------------------------------------------------
		c	= ();
		# --- Manually create colors
		c	= (c..., "rgb,1:red,0; green,0.44700; blue,0.74100");
		c	= (c..., "orange");
		c	= (c..., "violet");
		c	= (c..., "green!80!black");
		c	= (c..., "black");
		col  = c[index];
	else
		# ----------------------------------------------------
		# --- Automatic color generation
		# ----------------------------------------------------
		col = distinguishable_colors(size);#
		r	= Int(floor(col.r*255));
		b	= Int(floor(col.b*255));
		g	= Int(floor(col.g*255));
		col = "rgb,255:red,$r; green,$g; blue,$b";
	end
	return col;
end
export getColorLatex

function getMarkerLatex(ind)
  dict = ["square*","triangle*","diamond*","*","asterisk"];
  ll = length(dict)
  return dict[mod(ind-1,ll)+1];
end
export getMarkerLatex


function getSEP(snrRange,N)
	sep = zeros(Float64,length(snrRange));
	for k = 1 : 1 : N
		sep .+= binomial(N,k+1) *(-1)^(k+1) .* exp.( -N .* snrRange.* k ./(k+1))
	end
	return sep./N;
end
export getSEP




end # module
